<?php
    echo('PHP Boolean<br>');
    $x = 5985;
    var_dump(is_int($x));

    $x = 59.85;
    var_dump(is_int($x));

    echo('<br><br>PHP Infinite<br>');
    $x = 1.9e411;
    var_dump($x);

    echo('<br><br>PHP NAN<br>');
    $x = acos(8);
    var_dump($x);

    echo('<br><br>PHP Numerical Strings<br>');
    $x = 5985;
    var_dump(is_numeric($x));
    echo('<br>');

    $x = "5985";
    var_dump(is_numeric($x));
    echo('<br>');

    $x = "59.85" + 100;
    var_dump(is_numeric($x));
    echo('<br>');

    $x = "Hello";
    var_dump(is_numeric($x));
?>