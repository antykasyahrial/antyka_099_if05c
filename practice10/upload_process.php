<?php
    $target_dir = "uploads/";
    $target_file = $target_dir.basename($_FILES['gambar']['name']);
    $error = false;
    $imagefiletype=strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if(isset($_POST['submit'])){
        $check = getimagesize($_FILES['gambar']['tmp_name']);
        if($check !== false){
            echo "File is an image - ".$check['mime'].".";
            $error = false;
        }else{
            echo "File is not an image.<br>";
            $error = false; 
        }
        if(file_exists($target_file)){
            echo "Sorry, file already exist.<br>";
            $error = true;
        }
        if($_FILES['gambar']['size'] > 500000){
            echo "Sorry, file is too large.<br>";
            $error = true;
        }
        if($imagefiletype != "jpg" && $imagefiletype != "png" && $imagefiletype != "jpeg" && $imagefiletype != "gif" ){
            echo "Sorry, only JPG, PNG, JPEG & GIF files are allowed.<br>";
            $error = true;
        }
        if($error == true){
            echo "Sorry, your file was not uploaded.<br>";
        }else{
            if(move_uploaded_file($_FILES['gambar']['tmp_name'],$target_file)){
                $x = $target_dir."".basename($_FILES['gambar']['name']);
                echo "The file ".$x." has been uploaded.<br>";
            }else{
                echo "Sorry, there was an error uploading your image.<br>";
            }
        }
    }
    
?>