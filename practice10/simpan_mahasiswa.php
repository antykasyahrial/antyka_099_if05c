<?php
    include "koneksi.php";
    include "create_message.php";
    $target_dir = "uploads/";
    $target_file = $target_dir.basename($_FILES['gambar']['name']);
    $error = false;
    $imagefiletype=strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    
    if(isset($_POST['mahasiswa_id'])){
        $check = getimagesize($_FILES['gambar']['tmp_name']);
        if(file_exists($target_file)){
            $because =  "file already exist.";
            #create_message('Sorry, file already exist','danger','warning');
            $error = true;
        }
        if($_FILES['gambar']['size'] > 500000){
            $because = "file is too large.";
            #create_message('Sorry, file is too large','danger','warning');
            $error = true;
        }
        if($imagefiletype != "jpg" && $imagefiletype != "png" && $imagefiletype != "jpeg" && $imagefiletype != "gif" ){
            $because = "only JPG, PNG, JPEG & GIF files are allowed.<br>";
            #create_message('Sorry, only JPG, PNG, JPEG & GIF files are allowed','danger','warning');
            $error = true;
        }
        if($error == true){
            $pesan = "Sorry, your file was not uploaded, because ".$because;
            create_message($pesan,'danger','warning');
            header("location:index.php");
            exit();
        }else{
            if(move_uploaded_file($_FILES['gambar']['tmp_name'],$target_file)){
                $sql = "UPDATE mahasiswa SET nama_lengkap='".$_POST['nama_lengkap']."',
                alamat = '".$_POST['alamat']."',
                kelas_id= '".$_POST['kelas_id']."',
                foto = '".$target_file."' 
                WHERE mahasiswa_id = ".$_POST['mahasiswa_id'].";";
                #echo $sql;
                if($conn->query($sql) === TRUE){
                    $conn->close();
                    create_message('Ubah data berhasil','success','check');
                    header("location:index.php");
                    exit();
                }else{
                    #echo $sql;
                    $conn->close();
                    create_message('Error : '.$sql.'<br>'.$conn->error,'danger','warning');
                    header("location:index.php");
                    exit();
                }
                
            }else{
                #echo "Sorry, there was an error uploading your image.<br>";
                create_message('Sorry, there was an error uploading your image','danger','warning');
                header("location:index.php");
                exit();
            }
        }
            
    }else{
        $check = getimagesize($_FILES['gambar']['tmp_name']);
        if(file_exists($target_file)){
            $because =  "file already exist.";
            #create_message('Sorry, file already exist','danger','warning');
            $error = true;
        }
        if($_FILES['gambar']['size'] > 500000){
            $because = "file is too large.";
            #create_message('Sorry, file is too large','danger','warning');
            $error = true;
        }
        if($imagefiletype != "jpg" && $imagefiletype != "png" && $imagefiletype != "jpeg" && $imagefiletype != "gif" ){
            $because = "only JPG, PNG, JPEG & GIF files are allowed.<br>";
            #create_message('Sorry, only JPG, PNG, JPEG & GIF files are allowed','danger','warning');
            $error = true;
        }
        if($error == true){
            $pesan = "Sorry, your file was not uploaded, because ".$because;
            create_message($pesan,'danger','warning');
            header("location:index.php");
            exit();
        }else{
            if(move_uploaded_file($_FILES['gambar']['tmp_name'],$target_file)){
                $sql = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat,foto)
                VALUES ('".$_POST['nama_lengkap']."', ".$_POST['kelas_id'].", '".$_POST['alamat']."','".$target_file."')";
                if ($conn->query($sql) === TRUE) {
                    $conn->close();
                    create_message('Simpan data berhasil','success','check');
                    header("location:index.php");
                    exit();
                } else {
                    #echo "Error: " . $sql . "<br>" . $conn->error;
                    $conn->close();
                    create_message('Error : '.$sql.'<br>'.$conn->error,'danger','warning');
                    exit();
                }
                
            }else{
                #echo "Sorry, there was an error uploading your image.<br>";
                create_message('Sorry, there was an error uploading your image','danger','warning');
                header("location:index.php");
                exit();
            }
        }
        
    }
    
?>