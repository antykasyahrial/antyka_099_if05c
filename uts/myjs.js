function callModal(title,habitat,name,image,description){         
    $('#animalDetailModal').modal('show');         
    $( "#modal-title" ).html(title); 
    $( "#modal-habitat" ).html(habitat);         
    $( "#modal-name" ).html(name);         
    $( "#modal-description" ).html(description);         
    $( "#modal-img" ).attr('src',image);    
    $( "#modal-habitat" ).html(habitat);     
}

function callInputModal(title,name,habitat,image,description,animal_id){         
    $('#animalInputModal').modal('show');         
    $( "#modal-input-title" ).html(title);             
    $( "#modal-input-form-student-id" ).val(sessionStorage.getItem("nim"));         
    $( "#modal-input-form-title" ).val(name); 
    $( "#modal-input-form-habitat" ).val(habitat); 
    $( "#modal-input-form-animal-id" ).val(animal_id);        
    $( "#modal-input-form-image" ).val('');         
    $( "#modal-input-form-description" ).val(description);   
    if(title === "Add Animal"){
        $( "#modal-input-show-img" ).attr('src','');
    } else{
        $( "#modal-input-show-img" ).attr('src',image);
    }     
}


function readURL(input) {         
    if (input.files && input.files[0]) {           
        var reader = new FileReader();                     
        reader.onload = function(e) {             
            $('#modal-input-show-img').attr('src', e.target.result);           
            }           
            reader.readAsDataURL(input.files[0]);          
    }      
}

function insertAnimal(data){         
    $.ajax({             
        type: 'POST',             
        url: "http://uts.if05a.xyz/index.php/animal",                     
        dataType: "JSON",             
        data: new FormData(data),             
        processData: false,             
        contentType: false,             
        beforeSend: function( xhr ) {               
            xhr.overrideMimeType( "text/plain; charset=x-user-defined" );             
        },             
        error: function(xhr, status, error) {               
            console.log(xhr);             
        }           
    })          
    .done(function( data ) {             
        console.log(data); 
        swal("", "Data Added", "success");            
        getAnimals('http://uts.if05a.xyz/index.php/animal/q/student_id/'+sessionStorage.getItem("nim"),'My Animals');             
        $('#animalInputModal').modal('toggle');           
    });                
} 

function updateAnimal(data){         
    $.ajax({             
        type: 'POST',             
        url: "http://uts.if05a.xyz/index.php/animal/q/edit/1",                     
        dataType: "JSON",             
        data: new FormData(data),             
        processData: false,             
        contentType: false,             
        beforeSend: function( xhr ) {               
            xhr.overrideMimeType( "text/plain; charset=x-user-defined" );             
        },             
        error: function(xhr, status, error) {               
            console.log(xhr);             
        }           
    })          
    .done(function( data ) {             
        console.log(data);
        swal("", "Data Updated", "success");             
        getAnimals('http://uts.if05a.xyz/index.php/animal/q/student_id/'+sessionStorage.getItem("nim"));             
        $('#animalInputModal').modal('toggle');           
    });                
} 


function deleteAnimal(animal_id,animal_name){         
    var r = confirm("Delete "+animal_name+"?");         
    if (r) {           
        $.ajax({             
            type: 'DELETE',             
            url: "http://uts.if05a.xyz/index.php/animal/q/animal_id/"+animal_id,                     
            beforeSend: function( xhr ) {               
                xhr.overrideMimeType( "text/plain; charset=x-user-defined" );             
            },             
            error: function(xhr, status, error) {               
                console.log(xhr);             
            }           
        })           
        .done(function( data ) {             
            console.log(data);             
            swal("", "Data Deleted", "success");             
            getAnimals('http://uts.if05a.xyz/index.php/animal/q/student_id/'+sessionStorage.getItem("nim"),'My Animals');                       
        });         
    }             
}


function getAnimals(url,title){
var item = $( "#animal-item" );
$("#title-page").html(title);
$( ".animal-item" ).remove();
item.appendTo("#animal-items");
$.ajax({
    type: 'GET',
    url: url,        
    beforeSend: function( xhr ) {
    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    },
    error: function(xhr, status, error) {
    console.log(xhr);
    }
})
.done(function( data ) {
    myData = JSON.parse(data);
    for (i = 0; i < myData['animal'].length; i++) {
    var item = $( "#animal-item" ).clone();
    item.find('#card-description').html(myData['animal'][i].description);
    item.removeAttr( "style" );
    item.find('#card-img').attr('src', myData['animal'][i].image);
    item.find('#card-title').html(myData['animal'][i].animal_name);
    item.find('#card-habitat').html(myData['animal'][i].habitat);
    item.find('#card-name').html(myData['animal'][i].student_name + " (" + myData['animal'][i].class_name + ")");
    item.find('#card-date').html(myData['animal'][i].updated);
    item.find('#view-detail').attr("onClick","callModal(\""+myData['animal'][i].animal_name+"\",\"" +myData['animal'][i].habitat+"\",\""
                +myData['animal'][i].student_name+ " (" + myData['animal'][i].class_name + ")\",\""
                +myData['animal'][i].image+"\",\""+myData['animal'][i].description.replace('"','\\"').replace(/(\r\n|\n|\r)/gm,'<br>')+"\")"); 
    item.find('#edit-detail').attr("onClick","callInputModal(\"Edit Animal\",\""
        +myData['animal'][i].animal_name+"\",\""
        +myData['animal'][i].habitat+"\",\"" +myData['animal'][i].image+"\",\""
        +myData['animal'][i].description.replace('"','\\"').replace(/(\r\n|\n|\r)/gm,'<br>')+"\",\""
        +myData['animal'][i].animal_id+"\")");

    item.find('#delete-animal').attr("onClick","deleteAnimal("+myData['animal'][i].animal_id+",'"+myData['animal'][i].animal_name+"')"); 

    if(sessionStorage.getItem("nim")!=myData['animal'][i].student_id){
        item.find('#edit-detail').attr("style","display:none;");
        item.find('#delete-animal').attr("style","display: none;"); 
    }

    item.appendTo("#animal-items");
    }
});           
}

function getProfil(){
var url ="http://uts.if05a.xyz/index.php/student/q/student_id/17102099"
$.ajax({
    type : 'GET',
    url : url,
    beforeSend: function(xhr){
        xhr.overrideMimeType("text/plain; charset=x-user-defined");
    },
    error : function(xhr,status,error){
        console.log(xhr);
    }
})
.done(function(data){
    myData = JSON.parse(data);
    $('#name').text(myData['student'][0]['name']);
    $('#student_id').text(myData['student'][0]['student_id']);
    $('#class_name').text(myData['student'][0]['class_name']);
    sessionStorage.setItem("nim",myData['student'][0]['student_id'])
    
})
}

function search(){
$("#searchform").submit(function() {
    
    var url = "http://uts.if05a.xyz/index.php/animal/q/search/"+$("#input_search").val();
    var item = $( "#animal-item" );
    $("#title-page").html("Search Result");
    $( ".animal-item" ).remove();
    item.appendTo("#animal-items");
    $.ajax({
        type: 'GET',
        url: url,        
        beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
        },
        error: function(xhr, status, error) {
        console.log(xhr);
        console.log('a');
        }
    })
    .done(function( data ) {
        myData = JSON.parse(data);
        for (i = 0; i < myData['animal'].length; i++) {
        var item = $( "#animal-item" ).clone();
        item.find('#card-description').html(myData['animal'][i].description);
        item.removeAttr( "style" );
        item.find('#card-img').attr('src', myData['animal'][i].image);
        item.find('#card-title').html(myData['animal'][i].animal_name);
        item.find('#card-habitat').html(myData['animal'][i].habitat);
        item.find('#card-name').html(myData['animal'][i].student_name + " (" + myData['animal'][i].class_name + ")");
        item.find('#card-date').html(myData['animal'][i].updated);
        item.find('#view-detail').attr("onClick","callModal(\""+myData['animal'][i].animal_name+"\",\"" +myData['animal'][i].habitat+"\",\""
                    +myData['animal'][i].student_name+ " (" + myData['animal'][i].class_name + ")\",\""
                    +myData['animal'][i].image+"\",\""+myData['animal'][i].description.replace('"','\\"').replace(/(\r\n|\n|\r)/gm,'<br>')+"\")"); 
        
        item.find('#edit-detail').attr("onClick","callInputModal(\"Edit Animal\",\""
                    +myData['animal'][i].animal_name+"\",\""
                    +myData['animal'][i].habitat+"\",\"" +myData['animal'][i].image+"\",\""
                    +myData['animal'][i].description.replace('"','\\"').replace(/(\r\n|\n|\r)/gm,'<br>')+"\",\""
                    +myData['animal'][i].animal_id+"\")");

        item.find('#delete-animal').attr("onClick","deleteAnimal("+myData['animal'][i].animal_id+",'"+myData['animal'][i].animal_name+"')"); 

        if(sessionStorage.getItem("nim")!=myData['animal'][i].student_id){
            item.find('#edit-detail').attr("style","display:none;");
            item.find('#delete-animal').attr("style","display: none;"); 
        }
        
        item.appendTo("#animal-items");
        }
    });
});
}

$(document).ready(function(){
    getAnimals('http://uts.if05a.xyz/index.php/animal/q/student_id/'+sessionStorage.getItem("nim")  ,'My Animals')
    getProfil(); 
    search();

    $("#modal-input-form-image").change(function() {         
        readURL(this);       
    }); 

    $('#modal-input-form').submit(function (e) {           
        e.preventDefault();           
        var formData = $(this).serializeArray();              
        if(formData[0].value==""){           
            insertAnimal(this);           
        }           
        else{   
            updateAnimal(this);           
        }        
    }); 

});
