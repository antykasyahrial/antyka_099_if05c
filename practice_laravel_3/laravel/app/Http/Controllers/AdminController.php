<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
class AdminController extends Controller
{
    public function index(){
        return view('login.login');
    }

    public function process(Request $request){
        $validate = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $result = Admin::where('username','=',$validate['username'])->first();
        if($result){
            if(($request->password == $result->password)){
                session(['username' => $request->username]);
                return redirect('/adminlte/student');
            }else{
                return back()->withInput()->with('pesan','Login Gagal');
            }
        }else{
            return back()->withInput()->with('pesan','Login Gagal');
        }

    }
    public function logout(){
        session()->forget('username');
        return redirect('login')->with('pesan','Logout Berhasil');
    }
}
