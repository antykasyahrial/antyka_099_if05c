<?php

namespace App\Http\Controllers;
use App\Student;
use Illuminate\Http\Request;
use File;
class AdminLTEStudentController extends Controller
{
    //
    public function create(){
        $data['module']['name'] ="Tambah Mahasiswa";
        return view('adminlte.student.create',['data' => $data]);
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'nim'           => 'required|size:8, unieque:students',
            'nama'          => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan'       => 'required',
            'alamat'        => '',
            'image'         => 'required|file|image|max:1000',
        ]);
        $mahasiswa = new Student();
        $mahasiswa->nim = $validateData['nim'];
        $mahasiswa->name = $validateData['nama'];
        $mahasiswa->gender = $validateData['jenis_kelamin'];
        $mahasiswa->departement = $validateData['jurusan'];
        $mahasiswa->address = $validateData['alamat'];
        if($request->hasFile('image')){
            $ext = $request->image->getClientOriginalExtension();
            $name = 'user-'.time().".".$ext;
            $path = $request->image->move('asset/images',$name);
            $mahasiswa->image = $path;
        }
        $mahasiswa->save();
        $request->session()->flash('pesan', 'Penambahan data berhasil');
        return redirect()->route('adminlte.student.index');
    }

    public function index(){
        $data['module']['name'] ="Data Mahasiswa";
        $mahasiswa = Student::all();
        return view('adminlte.student.index',['students'=>$mahasiswa,'data'=>$data]);
    }

    public function show($nim){
        $data['module']['name'] ="Detail Mahasiswa";
        $result = Student::findOrFail($nim);
        return view('adminlte.student.show',['student' => $result,'data'=>$data]);
    }

    public function edit($student_id){
        $data['module']['name'] ="Edit Mahasiswa";
        $result = Student::findOrFail($student_id);
        return view('adminlte.student.edit',['student'=>$result,'data'=>$data]);
    }

    public function update(Request $request, Student $student){
        $validateData = $request->validate([
            'nim'           => 'required|size:8, unieque:students',
            'nama'          => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan'       => 'required',
            'alamat'        => '',
            'image'         => 'file|image|max:1000',
        ]);

        $student->nim = $validateData['nim'];
        $student->name = $validateData['nama'];
        $student->gender = $validateData['jenis_kelamin'];
        $student->departement = $validateData['jurusan'];
        $student->address = $validateData['alamat'];
        if($request->hasFile('image')){
            $ext = $request->image->getClientOriginalExtension();
            $name = 'user-'.time().".".$ext;
            File::delete($student->image);
            $path = $request->image->move('asset/images',$name);
            $student->image = $path;
        }
        $student->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');
        return redirect()->route('adminlte.student.show',['student'=>$student->id]);
    }

    public function destroy(Request $request, Student $student){
        File::delete($student->image);
        $student->delete();
        $request->session()->flash('pesan', 'Hapus data berhasil');
        return redirect()->route('adminlte.student.index');
    }
}
