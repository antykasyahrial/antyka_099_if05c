<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equip='X-UA-Compatible' content="ie=edge">
        <link href="../../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <title>Biodata {{$student->name}}</title>
    </head>

    <body>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="pt-3 d-flex justify-content-end align-items-center">
                    <h1 class="h2 mr-auto">Biodata {{$student->name}}</h1>
                    <a href=" {{ route('student.index') }}" class="btn btn-primary">
                    <img src="{{ URL::to('/asset/home.png') }}"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href=" {{ route('student.edit', ['student' => $student->id]) }}" class="btn btn-primary">
                    <img src="{{ URL::to('/asset/edit.png') }}"></a>
                    <form action="{{ route('student.destroy',['student' => $student->id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger ml-3">
                            <img src="{{ URL::to('/asset/delete.png') }}"></button>
                    </form>
                </div>

                <hr>
                @if(session()->has('pesan'))
                <div class="alert alert-success">{{ session()->get('pesan')}}</div>
                @endif
                <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Jurusan</th>
                    <th>Alamat</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$student->nim}}</td>
                <td>{{$student->name}}</td>
                <td>{{$student->gender=='P'?'Perempuan':'Laki-laki'}}</td>
                <td>{{$student->departement}}</td>
                <td>{{$student->address=='' ? 'N/A': $student->address}}</td>
            </tr>
            </div>
        </div>
    </div>
    </body>
</html>