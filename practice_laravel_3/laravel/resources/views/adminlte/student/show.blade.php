@extends('adminlte.student.admin_layout.app')
@section('header')
    @include('adminlte.student.admin_layout.header')
@endsection
@section('leftbar')
    @include('adminlte.student.admin_layout.leftbar')
@endsection
@section('rightbar')
    @include('adminlte.student.admin_layout.rightbar')
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Biodata {{$student->name}}</h1>
        </section>
        <section class="content">
        <div class="container mt-3">
                <div class="row">
                    <div class="col-12">
                        <div class="pt-3 d-flex justify-content-end align-items-center">
                            
                            <a href=" {{ route('adminlte.student.index') }}" class="btn btn-primary">
                            <img src="{{ URL::to('/asset/home.png') }}"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href=" {{ route('adminlte.student.edit', ['student' => $student->id]) }}" class="btn btn-primary">
                            <img src="{{ URL::to('/asset/edit.png') }}"></a>
                            <form action="{{ route('adminlte.student.destroy',['student' => $student->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger ml-3">
                                    <img src="{{ URL::to('/asset/delete.png') }}"></button>
                            </form>
                        </div>

                        <hr>
                        @if(session()->has('pesan'))
                        <div class="alert alert-success">{{ session()->get('pesan')}}</div>
                        @endif
                        <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nim</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Jurusan</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$student->nim}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->gender=='P'?'Perempuan':'Laki-laki'}}</td>
                        <td>{{$student->departement}}</td>
                        <td>{{$student->address=='' ? 'N/A': $student->address}}</td>
                    </tr>
            </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection