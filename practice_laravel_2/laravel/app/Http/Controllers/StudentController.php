<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function create()
    {
        return view('student.create');
    }
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nim'           => 'required|size:8,unique:students',
            'nama'          => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan'       => 'required',
            'alamat'        => '',
        ]);
        $mahasiswa = new Student();
        $mahasiswa->nim = $validate['nim'];
        $mahasiswa->name = $validate['nama'];
        $mahasiswa->gender = $validate['jenis_kelamin'];
        $mahasiswa->department = $validate['jurusan'];
        $mahasiswa->address = $validate['alamat'];
        $mahasiswa->save();
        $request->session()->flash('pesan','Penambahan Data Berhasil');
        return redirect()->route('student.index');
        
    }
    public function index(){
        $mahasiswas = Student::all();
        return view('student.index',['mahasiswas' => $mahasiswas]);
    }
    public function show($nim){
        $result = Student::findOrFail($nim);
        return view('student.show',['student' => $result]);
    }
    public function edit($nim){
        $result = Student::findOrFail($nim);
        return view('student.edit',['student' => $result]);
    }
    public function update(Request $request, Student $student)
    {
        $validate = $request->validate([
            'nim'           => 'required|size:8,unique:students',
            'nama'          => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'jurusan'       => 'required',
            'alamat'        => '',
        ]);
        $student->nim = $validate['nim'];
        $student->name = $validate['nama'];
        $student->gender = $validate['jenis_kelamin'];
        $student->department = $validate['jurusan'];
        $student->address = $validate['alamat'];
        $student->save();
        $request->session()->flash('pesan','Perubahan Data Berhasil');
        return redirect()->route('student.show',['student' => $student->id]);
        
    }
    public function destroy(Request $request, Student $student){
        $student->delete();
        $request->session()->flash('pesan','Hapus Data Berhasil');
        return redirect()->route('student.index');
    }
        
    
}
